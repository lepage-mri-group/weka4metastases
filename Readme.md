# weka4metastases

**weka4metastases** provides simple tools to automate detection and quantification of metastases/tumors on digitized histological slides.


![weka4metastases](Image/weka4metastases.png)


## Description of files

ImageJ ijm script files:

| **Filename** | **Description** |
| --- | --- |
| `WSI_2_Tiles.ijm` | Generate n x n tiles from user's selected ROI(s) in a digital whole slide image |
| `TWS metastases.ijm` | Perform classification of pixels from tiles into six classes and generate an Excel file with Metastases surface calculation|

ImageJ model files:

| **Filename** | **Description** |
| --- | --- |
| TWS_metastases_classifier_4_eosinophilic_tissue.model | Classifier for eosinophilic staining |
| TWS_metastases_classifier.model | Classifier for darker staining |	

 
## Prerequesites

- To use the plugins, you need to have ImageJ or Fiji installed on your computer. Here's how to install [Fiji](https://imagej.net/Fiji/Downloads).
- You can use our [classifiers](https://gitlab.com/sherbrooke-mlepage/weka4metastases/tree/master/Classifiers) or you can create and save your own. To train your own clasifier, follow the instructions [here](https://imagej.net/Trainable_Weka_Segmentation). We recommend to resize and save whole slide images at the desired magnification before using them as an input for analysis. If you use our classifier, images should be at the same resolution as the images used to train the model (5x magnification).
- Our classifier uses the feature *Laplacian* that belong to the ImageScience. You can activate it by [enabling the ImageScience update site in the updater](https://imagej.net/Following_an_update_site).  


## Getting Started 

1.	Use the desktop shortcut to start Fiji.
2.	Install our plugins with ***Plugins*** -> ***Macro*** -> ***Install*** (the new plugins will appear at the bottom of the Plugins menu once you've restarted Fiji) or use ***Plugins*** -> ***Macro*** -> ***Run*** and select the plugin you want to run from the file open dialog.
3. 	To divide the resized whole slide images into tiles of n x n pixels use the `WSI_2_Tiles` plugin.
	-	Select the image you want to divide.
	-	Draw a region you want to divide into tiles using the tool of your choice (it can be the whole image).
	-	Add the region to the ROI manager that popped up.
	-	Repeat the two previous steps until all of your ROIs are drawn and added to the ROI manager.
	-	Click OK on the Action Required window once you're done. 
	-	For each ROis, select the folder (*“Output directory”*) where you want to save the tiles.
	-	Once the selection is made, a window will pop up to invite you to enter the desired tile size. By default, the size is set to 1024. Change the value if desired the button **OK**. 
	-	You might want to look into the *Output directory* to control for image division.

4. 	You can now proceed to pixel classification and metastases segmentation on those tiles with the TWS metastases plugin. 
	-	Select the menu command ***Plugins*** -> ***TWS metastases*** (or ***Plugins*** -> ***Macro*** -> ***Run*** and select `TWS_metastases.ijm` from the file open dialog).
	-	A new window will appear that will allow you to select the folder with the images (≥ 2) you want to work on (*“Input directory”*).
	-	Select the folder where you want to save the classified images (*“Output directory”*).
	-	Once the selection is made, a window *“Histogram Lister”* will pop up. By default, the bin count is set to 256. Press the button **OK**. This is necessary to do the pixel calculation in each class for the current stack.
	-	The program will load the images from the selected *“Input directory”* and automatically create a stack of images which will appear in a new window named *“Stack”*.
	-	Then, the main GUI of the plugin, *“Trainable Weka Segmentation vx.x.xx”*, as well as the *“Log”* window will pop up. 
	-	The *“Log”* window will display messages about the images being processed and the classifier being loaded. When the classifier is loaded you will see in the log its parameters as well as the classes in which the pixels will be classified.
	-	While images are being processed, a progress bar will be displayed in the lower right hand corner of the main Fiji window and on the left hand corner updates on image processing. The *“Log”* window will also display updates on the images being processed.
	-	At the end of the classification, the windows *“Classified image.tif”* and *“Results”* will pop-up. The *“Classified images.tif”* is a stack of the classified images. You can navigate through that stack and the stack in the main GUI to compare and inspect the classification. The *“Results”* window consist of a table with, for each images, the number of pixels attributed to the *Metastases* and *Whole Brain* classes and the corresponding *Metastases area* calculation.
	-	In the log window, you will have a confirmation that all the images have been classified and the time it took to process the stack of images. Finally you will see the number of pixels attributed to each classe and where the results files (stack of classified images and excel file) have been saved.  
	-	In the chosen *“Output directory”*, you will find the stack of classified images and an xls file *“Classified image_results.xls”* containing the same data seen in the “Results”. You will be able to do post processing from those files.


### Example

You can test `WSI_2_Tiles.ijm` and `TWS_metastases.ijm` with images provided in the Whole slide image and Tiles folder respectively (in the *Test dataset* folder).


## BUGS

Please report any bugs to <dina.sikpa@usherbrooke.ca>