// Initialisation

requires("1.34m");
inputDirectory = getDirectory("Input directory");
outputDirectory = getDirectory("Output directory");
fileList = getFileList(inputDirectory);
imageExt = ".bmp";  
imageClassifier = File.openDialog("Select a classifier");
Dialog.create("Histogram Lister");
Dialog.addNumber("Number of Bins:", 256);
Dialog.show();
nBins = Dialog.getNumber();

// Opening batch of images

openImagesInDirectory(inputDirectory);

// Create a volume of images

run("Images to Stack", "name=Stack title=[] use");

// Trainable Weka Segmentation

run("Trainable Weka Segmentation");
wait(3000);
twsWindowTitle = getTitle();
selectWindow(twsWindowTitle);
call("trainableSegmentation.Weka_Segmentation.loadClassifier", imageClassifier);
call("trainableSegmentation.Weka_Segmentation.getResult");

// Create segmented image histogram and export pixels values into an Excel table

selectWindow("Classified image");
setBatchMode(true);
nMetastasesPixel=0;
nWholeBrainPixel=0; 
run("Clear Results");
if (bitDepth==8) {
      for (iSlice=1; iSlice<=nSlices; iSlice++) {
          if (nSlices>1) 
          run("Set Slice...", "slice=" + iSlice);
          getHistogram(value,count,nBins);
              setResult("Tile", iSlice-1, fileList[iSlice-1]);
              setResult("Metastases", iSlice-1, count[0]);
              setResult("Whole Brain", iSlice-1, count[0] + count[1] + count[2] + count [3]);
              nMetastasesPixel += count[0];
              nWholeBrainPixel += count[0] + count[1] + count[2] + count [3];
              setResult("Metastases area", iSlice-1, nMetastasesPixel / nWholeBrainPixel);
              print("Metastases :", nMetastasesPixel);
              print("Whole brain :", nWholeBrainPixel);
	}
} else {
      setBatchMode(true);
      stack = getImageID();
      for (iSlice=1; iSlice<=nSlices; iSlice++) {
          selectImage(stack);
          if (nSlices>1) run("Set Slice...", "slice=" + iSlice);
          run("Duplicate...", "title=temp");
          getHistogram(value,count,nBins);
          close();
              setResult("Tile", iSlice-1, fileList[iSlice-1]);
              setResult("Metastases", iSlice-1, count[0]);
              setResult("Whole Brain", iSlice-1, count[0] + count[1] + count[2] + count [3]);
              nMetastasesPixel += count[0];
              nWholeBrainPixel += count[0] + count[1] + count[2] + count [3];
              setResult("Metastases area", iSlice-1, nMetastasesPixel / nWholeBrainPixel);
              print("Metastases :", nMetastasesPixel);
              print("Whole brain :", nWholeBrainPixel);
      }
  }
  
	setOption("ShowRowNumbers", false);
	updateResults();
	run("Set Measurements...", "area mean min redirect=None decimal=9");
	imageName=getTitle; 
	saveAs("Results", outputDirectory + imageName + "_results.xls");
	print("Save to: " + outputDirectory);
	selectWindow("Classified image");
	saveAs("Tiff", outputDirectory + "Classified images.tif");	

function openImagesInDirectory(inputDirectory) {
       for (i = 0; i < fileList.length; i++) {
        if(File.isDirectory(inputDirectory + fileList[i]))   
            openImagesInDirectory("" + inputDirectory + fileList[i]);
        if(endsWith(fileList[i], imageExt))
            print("Processing: " + inputDirectory + fileList[i]);
            open(inputDirectory + fileList[i]);  //open image            
    }
}