// Opening an image
open(File.openDialog("Select an Image"));
fullImage = getImageID();

// Draw ROI in the image of interest
run("ROI Manager...");
waitForUser("Draw an ROI, then add it to the ROI manager. \n Repeat for all of your ROI, then press OK");
nRoi = roiManager("count");

for(iRoi = 0; iRoi < nRoi; iRoi++){
	selectImage(fullImage);
	roiManager("Select", iRoi);
	run("Duplicate...", " ");
	if(selectionType() !=-1){
		setBackgroundColor(255, 255, 255);
		run("Clear Outside");
	}
	imageId = getImageID(); 							// ROI parameters
	fullImageTitle = getTitle();
	outputDirectory = getDirectory("Output directory");
	
	tileLengthPixel = getNumber("Enter tile size", 1024);		// Tile size (tileLengthPixel x tileLengthPixel)

	getLocationAndSize(locX, locY, sizeW, sizeH); 
	imageWidth = getWidth(); 
	imageHeight = getHeight(); 
	targetTileWidth = tileLengthPixel; 
	targetTileHeight = tileLengthPixel;

	isYlimReached = 1;
	iTileY = 0;
	isXlimReached = 1; 
	iTileX = 0;

	while (isXlimReached) {						// ROI division into tiles and saving in the output directory
		isYlimReached = 1;
		iTileY=0;
		offsetX = iTileX*targetTileWidth;
		tileWidth = targetTileWidth;
		if ((offsetX + tileWidth) > imageWidth) {
				 tileWidth = imageWidth - offsetX;
				 isXlimReached = 0;
			}
		while (isYlimReached) {
		offsetY = iTileY*targetTileHeight;
		tileHeight = targetTileHeight;
		if ((offsetY + tileHeight) > imageHeight) {
				 tileHeight = imageHeight - offsetY;
				 isYlimReached = 0;
			}
			selectImage(imageId); 
			 call("ij.gui.ImageWindow.setNextLocation", locX + offsetX, locY + offsetY); 
			tileTitle = fullImageTitle + " [" + iTileX + "," + iTileY + "]"; 
			 run("Duplicate...", "title=&tileTitle"); 
			makeRectangle(offsetX, offsetY, tileWidth, tileHeight); 
			run("Crop"); 
			selectWindow(tileTitle);
			saveAs("bmp", outputDirectory+tileTitle);
			iTileY++;
		}
		iTileX++;
	}	
	selectImage(imageId); 
	close();
}

// Close all 
while (nImages>0) { 
          selectImage(nImages); 
          close(); 
}      

windowTitleList = getList("window.titles"); 
     for (iWindow=0; iWindow<windowTitleList.length; iWindow++){
     windowName = windowTitleList[iWindow];
    	selectWindow(windowName);
     run("Close");
} 
